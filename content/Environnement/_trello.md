---
title: "Trello"
weight: 5
date: 2022-01-11T14:19:27Z
draft: false
---

### Création de l'espace de travail et du tableau de gestion de projet
Trello est un outil de gestion de projet qui permet de planifier, coordonner et faire le suivi d'un projet. C'est un outil "léger", très facile à utiliser. Il est tout a fait indiqué pour la gestion d'un projet agile.  
Avec Trello, on crée un espace de travail collaboratif. Tous les membres de l'équipe partagent cet espace de travail.  
Dans cet espace de travail, on crée un ou des tableaux servant à faire le suivi de projets. 
Un tableau est simplement une série de tâches à effectuer, regroupées en listes. Exemple de listes:  
* Tâches à faire  
* Tâches assignées  
* Tâches complétées

## Marche à suivre pour la configuration de Trello

### (Tous) - Inscription à Trello

1. Inscrivez-vous à [Trello.com ](http://Trello.com)avec **votre adresse courriel du Collège et votre nom complet.**
2. Lorsqu'on vous demande le nom de l'espace de travail, conservez celui proposé par défaut.
3. Ajoutez votre photo à votre profil, une photo où on voit bien votre visage (en gros plan).
4. Vous avez un cellulaire? Installez-y l'application mobile Trello !
5. Ne créez pas de tableau pour le cours: vous utiliserez plutôt un lien vers un modèle tableau déjà structuré, déjà partagé avec moi.

### (Un seul membre de l'équipe) - Création d'un espace de travail

1. _Page d'accueil/Menu de gauche/Espaces de travail **+**_
2. Nom de l'espace de travail: **Équipe X**, Type d'espace de travail: Ingénierie/informatique, _Continuer_
3. Entrez l'adresse de vos coéquipiers, _Inviter dans l'espace de travail_
4. Ajoutez le professeur à votre espace de travail

Les membres de votre équipe devraient maintenant voir le groupe de travail sur la page d'accueil de Trello.

### (Un seul membre de l'équipe) - Création d'un tableau pour le projet CRTP

1. Ouvrez [ce tableau](https://trello.com/b/Yhq8pvm2). C'est le modèle du tableau que vous utiliserez pour la gestion de votre projet.
2. Faites-en une copie:

* _Menu du tableau (à droite)/... Plus/Copier le tableau_
* Nommer votre tableau **Projet CRTP Équipe X,** _créer_

Les membres de votre équipe devraient maintenant voir le tableau dans leur espace de travail.

### (Les autres membres de l'équipe) - Rejoindre le tableau du projet

Vous voyez maintenant dans l'espace de travail **Équipe X **le tableau du projet. Ceci est seulement une "invitation", il faut accepter l'invitation:

1. Allez dans l'espace de travail **Équipe X**
2. Dans la barre d'outils du tableau, cliquez sur le bouton _Rejoindre le tableau_. Vous pouvez maintenant modifier le tableau.